#!/usr/bin/env python3

"""
Analyze capture data in JSON format.

Usage:
  analyze.py <path> [options]

Options:
  -h --help         Show this screen.

  --interface <int> The interface to use
  --interfaces      List the interfaces in document

  --hide-out        Hides output data
  --hide-in         Hides input data
"""

import json
import re

from docopt import docopt

response_map = {}  # Maps the response data to
global last


def bytes_len(msg):  # Checks how long the bytes list is ignoring the extra bytes
    out = 0
    for i in range(0, len(msg)):
        if msg[i] != 0x00:
            out = i + 1
    return out


def parse_in(msg):
    if hide_in:  # Return if it shouldn't be printed
        return

    if not msg[0] == 0x00:  # Ensure we have the suspected prefix
        print(f'Unknown prefix for input: {msg[0:1]}')
        return

    # Load vars
    response_code = msg[1]
    response_data = msg[2:]

    response_to = response_map[response_code]  # Get what command this was in response to

    if response_to == 0x13:  # Read the firmware version
        print(f'In; Firmware version {response_data[1]}.{response_data[2]}.{response_data[3]}')
    elif response_to == 0x01:  # Some kind of fan status out
        if (response_data[0] == 0x00 and response_data[1] == 0x06 and response_data[2] == 0x00 and
                response_data[3] == 0x07):  # if it is fan speed
            pump_speed = int.from_bytes(response_data[4:6], byteorder='little')
            fan_speeds = []
            for i in range(0, int((bytes_len(response_data) - 6) / 2)):
                in_index = i * 2 + 6
                fan_speeds.append(int.from_bytes(response_data[in_index:in_index + 2], byteorder='little'))
            print(f'In; Pump Speed: {pump_speed}; Fan Speeds: {fan_speeds}')
        elif (response_data[0] == 0x00 and response_data[1] == 0x10 and response_data[2] == 0x00 and
              response_data[3] == 0x02 and response_data[4] == 0x00):  # should be temp
            water_temp = int.from_bytes(response_data[5:7], byteorder='little') / (10 ** response_data[7])
            print(f'In; Water temp: {water_temp}')
        else:  # don't know what to do with this status mesage
            pass
            print(f'Unknown; In; Response Code: {hex(response_code)}; Other status here with data: {response_data.hex(":")}')
    elif response_to == 0x00:  # RGB data out
        print(f'Unknown; In;  Response Code: {hex(response_code)}; Some response to RBG with data: {response_data.hex(":")}')
    else:  # Still unknown
        print(f'Unknown; In; Response to Command: {hex(response_to)}; Response code: {hex(response_code)}; '
              f'Command Data: {response_data.hex(":")}')


def parse_out(msg):
    if not msg[0] == 0x08:  # Check the suspicion for the prefix
        print(f'Unknown prefix for output: {msg[0:1]}')
        return

    # Get vars
    response_code = msg[1]
    command = msg[2]
    command_data = msg[3:]

    # Set response in the map so we can confirm the match
    response_map[response_code] = command

    if hide_out:  # Return if we don't want output
        return

    if command == 0x13:  # Get Firmware
        print(f'Out; Get Firmware')
    elif command == 0x01:
        if bytes_len(command_data) == 0:  # Get fan status as that is the full command
            print(f'Out; Get fan status')
        else:
            print(f'Out; Unknown; Set fan speed; Response Code: {hex(response_code)}; Data: {command_data.hex(":")}')
    elif command == 0x00:
        if bytes_len(command_data) == 0:  # Get RGB status of some kind
            print(f'Unknown; Out; RGB; Response Code: {hex(response_code)}; Command Data: {command_data.hex(":")}')
        else:
            rgb_len = int.from_bytes(command_data[0:2], byteorder='little')
            rgb_data = command_data[4:]
            rgb_values = []
            if bytes_len(rgb_data) != rgb_len:  # if the bytes legend is different the data says we have idk what it is
                print(f'Unknown; Out; RGB; Response Code: {hex(response_code)}; Command Data: {command_data.hex(":")}')
                return

            # This command should be our full RGB for all fans
            for i in range(2, rgb_len, 3):
                rgb_values.append(f'#{hex(int.from_bytes(rgb_data[i:i + 3], byteorder="big"))[2:]}')
            print(f'Out; Set RGB; Len: {rgb_len}; RGB Len: {len(rgb_values)} ; RGB Values: {rgb_values}')
    else:  # Still unknown
        print(f'Unknown; Out; Response code: {hex(response_code)}; Command: {hex(command)}; '
              f'Command Data: {command_data.hex(":")}')


def get_interfaces():
    interfaces = set()
    for msg in capture:
        interfaces.add(int(msg['_source']['layers']['frame']['frame.interface_id']))
    print(f'Interfaces: {interfaces}')


if __name__ == '__main__':
    args = docopt(__doc__)

    hide_in = args['--hide-in']
    hide_out = args['--hide-out']
    interface = int(args['--interface'])

    with open(args['<path>'], 'r') as f:
        capture = json.load(f)

    if args['--interfaces']:
        get_interfaces()
        exit()

    for item in capture:
        layers = item['_source']['layers']
        if 'Setup Data' in layers:
            continue
        elif 'usbhid' in layers:
            continue
        elif 'usbhid.data' in layers:  # Limited to only the data messages
            if interface is None or interface == int(layers['frame']['frame.interface_id']):
                data = bytes.fromhex(
                    (
                            re.sub(r'(?:\:00)*$', '', layers['usbhid.data']) +  # Trim most of the zeros from the end
                            ":00:00:00:00:00:00:00:00:00:00"  # Add 10 back to avoid processing errors
                    ).replace(':', '')  # Replace for convention to python bytes
                )
                if layers['usb']['usb.src'] == 'host':
                    parse_out(data)
                else:
                    parse_in(data)
        elif 'STRING DESCRIPTOR' in layers:
            continue
        elif 'DEVICE DESCRIPTOR' in layers:
            continue
        elif 'CONFIGURATION DESCRIPTOR' in layers:
            continue
        elif len(layers.keys()) > 2:
            print(f'Missing handler for pack with layers: {layers.keys()}')
